import numpy as np
import torch
from sklearn import metrics
from torch import nn
import torch.nn.functional as F
from torch.utils.data import DataLoader

from zad1 import calculate_frequencies, Vocab, NLPDataset, pad_collate_fn, generate_embedding_matrix


class Baseline(nn.Module):

    def __init__(self, embedding):
        super(Baseline, self).__init__()

        self.embedding = embedding
        self.fc1 = nn.Linear(300, 150)
        self.fc2 = nn.Linear(150, 150)
        self.fc3 = nn.Linear(150, 1)

    def forward(self, x):

        x = self.embedding(x)
        x = torch.mean(x, dim=1)
        a1 = torch.relu(self.fc1(x))
        a2 = torch.relu(self.fc2(a1))

        return self.fc3(a2)


def load_dataset():
    batch_size = 10
    shuffle = True

    # train dataset loader
    frequencies_text, frequencies_label = calculate_frequencies("sst_train_raw.csv")

    text_vocab = Vocab(frequencies_text, max_size=-1, min_freq=0, flag=True)
    label_vocab = Vocab(frequencies_label, max_size=-1, min_freq=0, flag=False)

    train_dataset = NLPDataset("sst_train_raw.csv", frequencies_text, frequencies_label, text_vocab, label_vocab)

    train_data_loader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=shuffle,
                                   collate_fn=pad_collate_fn)

    embedding_matrix = generate_embedding_matrix("sst_glove_6b_300d.txt", text_vocab)

    # test dataset loader

    batch_size = 32

    test_dataset = NLPDataset("sst_test_raw.csv", frequencies_text, frequencies_label, text_vocab, label_vocab)

    test_data_loader = DataLoader(dataset=test_dataset, batch_size=batch_size, shuffle=shuffle,
                                  collate_fn=pad_collate_fn)

    # validate data loader

    validation_dataset = NLPDataset("sst_valid_raw.csv", frequencies_text, frequencies_label, text_vocab, label_vocab)

    validation_data_loader = DataLoader(dataset=validation_dataset, batch_size=batch_size, shuffle=shuffle,
                                        collate_fn=pad_collate_fn)

    return train_data_loader, validation_data_loader, test_data_loader, embedding_matrix


def train(model, data, optimizer, criterion):
    model.train()
    for batch_num, batch in enumerate(data):
        model.zero_grad()
        x = batch[0]
        y = batch[1].unsqueeze(1)
        logits = model(x.cuda())
        logits = logits.type(torch.double)
        y = y.type(torch.double)
        loss = criterion(logits.cuda(), y.cuda())
        loss.backward()
        # torch.nn.utils.clip_grad_norm_(model.parameters(), 0.25)
        optimizer.step()
        # print(f'step: {batch_num}, loss:{loss}')


def evaluate(model, data, criterion):
    cuda = torch.device('cuda')
    model.eval()
    correct = 0
    total = 0
    with torch.no_grad():
        for batch_num, batch in enumerate(data):
            x = batch[0]
            y = batch[1].unsqueeze(1).cuda()
            logits = model(x.cuda())
            logits = logits.type(torch.double)
            # y = y.type(torch.double)
            # loss = criterion(logits, y)
            predicted = torch.tensor([[0] if i < 0 else [1] for i in logits], device=cuda)

            total += y.size(0)
            correct += (predicted == y).sum().item()

    print('Accuracy of the network on the validation dataset: %d %%' % (100 * correct / total))


def main(args):
    cuda = torch.device('cuda')
    seed = args
    np.random.seed(seed)
    torch.manual_seed(seed)

    train_dataset, valid_dataset, test_dataset, embedding_matrix = load_dataset()
    model = Baseline(embedding_matrix)
    model = model.cuda()

    criterion = nn.BCEWithLogitsLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)

    for epoch in range(5):
        train(model, train_dataset, optimizer, criterion)
        evaluate(model, valid_dataset, criterion)

    correct = 0
    total = 0
    with torch.no_grad():
        for batch_num, batch in enumerate(test_dataset):
            x = batch[0]
            y = batch[1].unsqueeze(1).cuda()
            logits = model(x.cuda())
            logits = logits.type(torch.double)
            # y = y.type(torch.double)
            # loss = criterion(logits, y)
            predicted = torch.tensor([[0] if i < 0 else [1] for i in logits], device=cuda)

            total += y.size(0)
            correct += (predicted == y).sum().item()

    print('Accuracy of the network on the test dataset: %d %%' % (100 * correct / total))


if __name__ == "__main__":
    main(7052020)
