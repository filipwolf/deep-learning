from dataclasses import dataclass
import csv

import numpy.random
import torch
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import Dataset, DataLoader


def pad_collate_fn(batch, pad_index=0):
    """
    Arguments:
      batch:
        list of Instances returned by `Dataset.__getitem__`.
    Returns:
      A tensor representing the input batch.
      :param batch:
      :param pad_index:
    """

    texts, labels = zip(*batch)

    lengths = torch.tensor([len(text) for text in texts])

    texts = pad_sequence(texts, batch_first=True)

    return texts, torch.cat(labels), lengths


def generate_embedding_matrix(vector_embeddings, text_vocab):
    embedding_matrix = []

    for _ in range(len(text_vocab.stoi.keys())):
        embedding_matrix.append([numpy.random.standard_normal() for _ in range(300)])

    with open(vector_embeddings) as embeddings:
        lines = embeddings.readlines()

        for line in lines:
            elems = line.split()
            embedding_matrix[text_vocab.stoi[elems[0]]] = [float(item) for item in elems[1:]]

    return torch.nn.Embedding.from_pretrained(torch.Tensor(embedding_matrix), padding_idx=0, freeze=True)


def calculate_frequencies(csv_file):
    frequencies_text = {}
    frequencies_label = {'positive': 0, 'negative': 1}

    tempDict = {}

    with open(csv_file, newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            for word in row[0].split(' '):
                if word in tempDict.keys():
                    tempDict[word] += 1
                else:
                    tempDict[word] = 1

    sorted_list = sorted(tempDict.items(), key=lambda x: x[1], reverse=True)
    for item in sorted_list:
        frequencies_text[item[0]] = item[1]

    return frequencies_text, frequencies_label


@dataclass
class Instance:
    text: []
    label: str

    def __repr__(self):
        return f'Text: {self.text}, Label: {self.label!r})'


class NLPDataset(Dataset):

    def __init__(self, csv_file, frequencies_text, frequencies_label, text_vocab, label_vocab):
        self.instances = []
        self.frequencies_text = frequencies_text
        self.frequencies_label = frequencies_label
        self.text_vocab = text_vocab
        self.label_vocab = label_vocab

        with open(csv_file, newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                self.instances.append(Instance(row[0].split(' '), row[1].strip()))

    def __len__(self):
        return len(self.instances)

    def __getitem__(self, item):
        return self.text_vocab.encode(self.instances[item].text), self.label_vocab.encode(self.instances[item].label)


class Vocab:

    def __init__(self, frequencies, max_size, min_freq, flag):

        if flag:
            self.stoi = {'<PAD>': 0, '<UNK>': 1}
            self.itos = {0: '<PAD>', 1: '<UNK>'}
            cnt = 2
        else:
            self.stoi = {}
            self.itos = {}
            cnt = 0

        for word in frequencies.keys():
            if max_size != -1 and cnt == max_size or min_freq != 0 and cnt >= min_freq:
                break
            self.stoi[word] = cnt
            self.itos[cnt] = word
            cnt += 1

    def encode(self, tokens):

        tensorList = []
        if type(tokens) is str:
            tokens = [tokens]
        for word in tokens:
            if word not in self.stoi.keys():
                tensorList.append(1)
            else:
                tensorList.append(self.stoi[word])

        return torch.tensor(tensorList, dtype=torch.long)


if __name__ == "__main__":
    batch_size = 2
    shuffle = False

    frequencies_text, frequencies_label = calculate_frequencies("sst_train_raw.csv")

    text_vocab = Vocab(frequencies_text, max_size=-1, min_freq=0, flag=True)
    label_vocab = Vocab(frequencies_label, max_size=-1, min_freq=0, flag=False)

    train_dataset = NLPDataset("sst_train_raw.csv", frequencies_text, frequencies_label, text_vocab, label_vocab)
    instance = train_dataset.instances[3]
    print(instance)

    print(f"Numericalized text: {text_vocab.encode(instance.text)}")
    print(f"Numericalized label: {label_vocab.encode(instance.label)}")

    numericalized_text, numericalized_label = train_dataset[3]

    print(f"Numericalized text: {numericalized_text}")
    print(f"Numericalized label: {numericalized_label}")

    embedding_matrix = generate_embedding_matrix("sst_glove_6b_300d.txt", text_vocab)

    train_data_loader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=shuffle,
                                   collate_fn=pad_collate_fn)

    texts, labels, lengths = next(iter(train_data_loader))
    print(f"Texts: {texts}")
    print(f"Labels: {labels}")
    print(f"Lengths: {lengths}")
