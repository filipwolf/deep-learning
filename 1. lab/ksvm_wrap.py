import numpy as np
import sklearn.svm as svm
import matplotlib.pyplot as plt

import data


class KSVMWrap(svm.SVC):
    """
    Metode:
      __init__(self, X, Y_, param_svm_c=1, param_svm_gamma='auto'):
        Konstruira omotač i uči RBF SVM klasifikator
        X, Y_:           podatci i točni indeksi razreda
        param_svm_c:     relativni značaj podatkovne cijene
        param_svm_gamma: širina RBF jezgre

      predict(self, X)
        Predviđa i vraća indekse razreda podataka X

      get_scores(self, X):
        Vraća klasifikacijske mjere
        (engl. classification scores) podataka X;
        ovo će vam trebati za računanje prosječne preciznosti.

      support
        Indeksi podataka koji su odabrani za potporne vektore
    """

    def __init__(self, X, Y_, param_svm_c=1, param_svm_gamma='auto'):
        super(KSVMWrap, self).__init__()
        self.probability = True
        self.C = param_svm_c
        self.gamma = param_svm_gamma
        self.fit(X, Y_)

    def predict(self, X):
        return super(KSVMWrap, self).predict(X)

    def get_scores(self, X):
        return super(KSVMWrap, self).predict_proba(X)

    def support(self):
        return self.support_


def svm_decfun(model):
    def classify(X):
        return model.decision_function(X)

    return classify


if __name__ == "__main__":
    np.random.seed(100)

    X, Y_ = data.sample_gmm_2d(6, 2, 10)
    Yoh_ = data.class_to_onehot(Y_)

    svm = KSVMWrap(X, Y_)
    print(svm.get_scores(X))
    print(svm.support())

    Y = svm.predict(X)
    bla = np.array([svm.get_scores(X)[i][Y_[i]] for i in range(len(Y_))])

    accuracy, recall, precision = data.eval_perf_multi(Y_, Y)
    AP = data.eval_AP(Y_[bla.argsort()])
    print(accuracy, recall, precision, AP)

    decfun = svm_decfun(svm)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(svm.decision_function, bbox, offset=0.5)
    data.graph_data(X, Y_, Y, special=[])
    plt.show()
