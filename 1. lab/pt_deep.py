import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

import data


class PTDeep(nn.Module):
    def __init__(self, function, params):
        """Arguments:
           - D: dimensions of each datapoint
           - C: number of classes
        """
        super().__init__()
        self.weights = torch.nn.ParameterList([nn.Parameter(torch.tensor(np.random.randn(params[i], params[i + 1]), dtype=torch.float)) for i in range(len(params) - 1)])
        self.biases = torch.nn.ParameterList([nn.Parameter(torch.zeros(params[i + 1], dtype=torch.float)) for i in range(len(params) - 1)])
        self.function = function

    def forward(self, X):
        h = torch.clone(X)
        for i in range(len(self.weights) - 1):
            h = self.function(torch.mm(h, self.weights[i]) + self.biases[i])
        return torch.softmax(torch.mm(h, self.weights[-1]) + self.biases[-1], dim=1)

    def get_loss(self, X, Yoh_):
        return - torch.mean(torch.log(torch.sum(X * Yoh_, dim=1)))


def count_params(model):
    cnt = 0
    for i in model.named_parameters():
        cnt += np.prod(i[1].size())
    print(cnt)


def train(model, X, Yoh_, param_niter, param_delta, param_lambda):
    """Arguments:
       - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
       - param_delta: learning rate
    """
    optimizer = optim.SGD(model.parameters(), lr=param_delta, weight_decay=param_lambda)

    for i in range(param_niter):
        # afin regresijski model
        Y_ = model.forward(X)

        loss = model.get_loss(Y_, Yoh_)

        # računanje gradijenata
        loss.backward()

        # korak optimizacije
        optimizer.step()

        # Postavljanje gradijenata na nulu
        optimizer.zero_grad()

        print(f'step: {i}, loss:{loss}')


def eval(model, X):
    """Arguments:
       - model: type: PTLogreg
       - X: actual datapoints [NxD], type: np.array
       Returns: predicted class probabilites [NxC], type: np.array
    """

    return torch.Tensor.numpy(torch.Tensor.detach(model.forward(X)))


def logreg_decfun(model):
    def classify(X):
        return eval(model, torch.tensor(X))

    return classify


if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    np.random.seed(100)

    # instanciraj podatke X i labele Yoh_
    X1, Y_1 = data.sample_gmm_2d(4, 2, 40)
    Yoh_1 = data.class_to_onehot(Y_1)
    X = torch.tensor(X1, dtype=torch.float)
    Yoh_ = torch.tensor(Yoh_1)

    params = torch.nn.ParameterList([nn.Parameter(torch.tensor(i), requires_grad=False) for i in [2, 10, 10, 2]])

    # definiraj model:
    ptlr = PTDeep(torch.relu, params)

    # nauči parametre (X i Yoh_ moraju biti tipa torch.Tensor):
    train(ptlr, X, Yoh_, 10000, 0.05, 0.05)

    # dohvati vjerojatnosti na skupu za učenje
    probs = eval(ptlr, X)
    Y = np.array([np.argmax(i) for i in probs])
    accuracy, recall, precision = data.eval_perf_multi(Y_1, Y)
    print(accuracy, recall, precision)

    count_params(ptlr)

    decfun = logreg_decfun(ptlr)
    bbox = (np.min(X1, axis=0), np.max(X1, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X1, Y_1, Y, special=[])

    # show the plot
    plt.show()
