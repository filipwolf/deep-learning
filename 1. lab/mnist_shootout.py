import random

import numpy as np
import torch
import torchvision
import torch.optim as optim
import sklearn.model_selection as ms
import sklearn.svm as svm
import sklearn.utils as utils
import matplotlib.pyplot as plt


import data
from ksvm_wrap import svm_decfun
from pt_deep import PTDeep, train, eval


def validation():
    np.random.seed(100)
    torch.set_default_tensor_type('torch.cuda.FloatTensor')

    dataset_root = '/home/filip/data'
    mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=False)
    mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=False)

    x_train, y_train = mnist_train.data, mnist_train.targets
    x_test, y_test = mnist_test.data, mnist_test.targets
    x_train, x_test = x_train.float().div_(255.0), x_test.float().div_(255.0)

    N = x_train.shape[0]
    D = x_train.shape[1] * x_train.shape[2]
    C = y_train.max().add_(1).item()
    x_train = x_train.view(-1, 784)
    x_test = x_test.view(-1, 784)
    y_train_ = torch.tensor(data.class_to_onehot(y_train))
    y_test = torch.Tensor.numpy(torch.Tensor.detach(y_test))
    x_train = x_train.cuda()
    #x_test = x_test.cuda()
    #y_train_ = y_train_.cuda()

    params = torch.nn.ParameterList([torch.nn.Parameter(torch.tensor(i), requires_grad=False) for i in [784, 100, 10]])

    ptlr = PTDeep(torch.tanh, params)

    optimizer = optim.SGD(ptlr.parameters(), lr=0.5, weight_decay=0.05)
    x1, x2, y1, y2 = ms.train_test_split(x_train, y_train_, test_size=0.2)

    bestIter = 0
    minLoss = None

    for i in range(100):
        # afin regresijski model
        Y_ = ptlr.forward(x1)

        loss = ptlr.get_loss(Y_, y1)

        # računanje gradijenata
        loss.backward()

        # korak optimizacije
        optimizer.step()

        # Postavljanje gradijenata na nulu
        optimizer.zero_grad()

        print(f'step: {i}, loss:{loss}')

        Y_ = ptlr.forward(x2)

        validaton_loss = ptlr.get_loss(Y_, y2)

        if minLoss is None or minLoss > validaton_loss:
            minLoss = validaton_loss
            bestIter = i

    print(bestIter)

    ptlr = ptlr.cpu()
    # for i, j in ptlr.named_parameters():
    #     print(j)

    probs = eval(ptlr, x_test)
    Y = np.array([np.argmax(i) for i in probs])
    accuracy, recall, precision = data.eval_perf_multi(y_test, Y)
    print(accuracy, recall, precision)


def train_mb():
    np.random.seed(100)
    #torch.set_default_tensor_type('torch.cuda.FloatTensor')

    dataset_root = '/home/filip/data'
    mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=False)
    mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=False)

    x_train, y_train = mnist_train.data, mnist_train.targets
    x_test, y_test = mnist_test.data, mnist_test.targets
    x_train, x_test = x_train.float().div_(255.0), x_test.float().div_(255.0)

    N = x_train.shape[0]
    D = x_train.shape[1] * x_train.shape[2]
    C = y_train.max().add_(1).item()
    x_train = x_train.view(-1, 784)
    x_test = x_test.view(-1, 784)
    y_train_ = torch.tensor(data.class_to_onehot(y_train))
    y_test = torch.Tensor.numpy(torch.Tensor.detach(y_test))
    #x_train = x_train.cuda()
    #x_test = x_test.cuda()
    #y_train_ = y_train_.cuda()

    params = torch.nn.ParameterList([torch.nn.Parameter(torch.tensor(i), requires_grad=False) for i in [784, 100, 10]])

    ptlr = PTDeep(torch.tanh, params)

    optimizer = optim.SGD(ptlr.parameters(), lr=0.5)
    i = 0

    for _ in range(30):
        x_train, y_train_ = utils.shuffle(x_train, y_train_)

        x_batches = [i for i in torch.split(x_train, 60)]
        y_batches = [i for i in torch.split(y_train_, 60)]

        for x, y in zip(x_batches, y_batches):
            # afin regresijski model
            Y_ = ptlr.forward(x)

            loss = ptlr.get_loss(Y_, y)

            # računanje gradijenata
            loss.backward()

            # korak optimizacije
            optimizer.step()

            # Postavljanje gradijenata na nulu
            optimizer.zero_grad()

            print(f'step: {i}, loss:{loss}')
            i += 1

    ptlr = ptlr.cpu()
    # for i, j in ptlr.named_parameters():
    #     print(j)

    probs = eval(ptlr, x_test)
    Y = np.array([np.argmax(i) for i in probs])
    accuracy, recall, precision = data.eval_perf_multi(y_test, Y)
    print(accuracy, recall, precision)


def adam():
    np.random.seed(100)
    torch.set_default_tensor_type('torch.cuda.FloatTensor')

    dataset_root = '/home/filip/data'
    mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=False)
    mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=False)

    x_train, y_train = mnist_train.data, mnist_train.targets
    x_test, y_test = mnist_test.data, mnist_test.targets
    x_train, x_test = x_train.float().div_(255.0), x_test.float().div_(255.0)

    N = x_train.shape[0]
    D = x_train.shape[1] * x_train.shape[2]
    C = y_train.max().add_(1).item()
    x_train = x_train.view(-1, 784)
    x_test = x_test.view(-1, 784)
    y_train_ = torch.tensor(data.class_to_onehot(y_train))
    y_test = torch.Tensor.numpy(torch.Tensor.detach(y_test))
    x_train = x_train.cuda()
    # x_test = x_test.cuda()
    # y_train_ = y_train_.cuda()

    params = torch.nn.ParameterList(
        [torch.nn.Parameter(torch.tensor(i), requires_grad=False) for i in [784, 100, 100, 100, 10]])

    ptlr = PTDeep(torch.tanh, params)

    optimizer = optim.Adam(ptlr.parameters(), lr=1e-4, weight_decay=0.05)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=1-1e-4)

    for i in range(6000):
        # afin regresijski model
        Y_ = ptlr.forward(x_train)

        loss = ptlr.get_loss(Y_, y_train_)

        # računanje gradijenata
        loss.backward()

        # korak optimizacije
        optimizer.step()

        # Postavljanje gradijenata na nulu
        optimizer.zero_grad()

        scheduler.step()
        print(f'step: {i}, loss:{loss}')
        print(scheduler.get_lr())

    ptlr = ptlr.cpu()
    # for i, j in ptlr.named_parameters():
    #     print(j)

    probs = eval(ptlr, x_test)
    Y = np.array([np.argmax(i) for i in probs])
    accuracy, recall, precision = data.eval_perf_multi(y_test, Y)
    print(accuracy, recall, precision)


def svm_func():
    np.random.seed(100)
    #torch.set_default_tensor_type('torch.cuda.FloatTensor')

    dataset_root = '/home/filip/data'
    mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=False)
    mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=False)

    x_train, y_train = mnist_train.data, mnist_train.targets
    x_test, y_test = mnist_test.data, mnist_test.targets
    x_train, x_test = x_train.float().div_(255.0), x_test.float().div_(255.0)

    N = x_train.shape[0]
    D = x_train.shape[1] * x_train.shape[2]
    C = y_train.max().add_(1).item()
    x_train = x_train.view(-1, 784)
    x_test = x_test.view(-1, 784)
    y_train_ = torch.tensor(data.class_to_onehot(y_train))
    y_test = torch.Tensor.numpy(torch.Tensor.detach(y_test))
    #x_train = x_train.cuda()

    model = svm.LinearSVC(C=1)
    model.fit(x_train, y_train)

    Y = model.predict(x_test)

    accuracy, recall, precision = data.eval_perf_multi(y_test, Y)
    print(accuracy, recall, precision)

    model = svm.SVC(C=1, gamma='auto')
    model.fit(x_train, y_train)

    Y = model.predict(x_test)

    accuracy, recall, precision = data.eval_perf_multi(y_test, Y)
    print(accuracy, recall, precision)


if __name__ == "__main__":

    #train_mb()
    #validation()
    adam()
    #svm_func()

    # np.random.seed(100)
    # torch.set_default_tensor_type('torch.cuda.FloatTensor')
    #
    # dataset_root = '/home/filip/data'
    # mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=False)
    # mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=False)
    #
    # x_train, y_train = mnist_train.data, mnist_train.targets
    # x_test, y_test = mnist_test.data, mnist_test.targets
    # x_train, x_test = x_train.float().div_(255.0), x_test.float().div_(255.0)
    #
    # N = x_train.shape[0]
    # D = x_train.shape[1] * x_train.shape[2]
    # C = y_train.max().add_(1).item()
    # x_train = x_train.view(-1, 784)
    # x_test = x_test.view(-1, 784)
    # y_train_ = torch.tensor(data.class_to_onehot(y_train))
    # y_test = torch.Tensor.numpy(torch.Tensor.detach(y_test))
    # x_train = x_train.cuda()
    # #x_test = x_test.cuda()
    # #y_train_ = y_train_.cuda()
    #
    # params = torch.nn.ParameterList([torch.nn.Parameter(torch.tensor(i), requires_grad=False) for i in [784, 100, 10]])
    #
    # ptlr = PTDeep(torch.relu, params)
    #
    # train(ptlr, x_train, y_train_, 500, 0.05, 0.05)
    # ptlr = ptlr.cpu()
    # # for i, j in ptlr.named_parameters():
    # #     print(j)
    #
    # probs = eval(ptlr, x_test)
    # Y = np.array([np.argmax(i) for i in probs])
    # accuracy, recall, precision = data.eval_perf_multi(y_test, Y)
    # print(accuracy, recall, precision)
