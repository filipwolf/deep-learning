import numpy as np
import data


def fcann2_train(X, Y_):

    w1 = np.random.randn(2, 5)
    w1mean = np.mean(w1)
    w1std = np.std(w1)
    w1 = np.subtract(w1, w1mean)
    w1 = np.divide(w1, w1std)
    w2 = np.random.randn(5, max(Y_) + 1)
    w2mean = np.mean(w2)
    w2std = np.std(w2)
    w2 = np.subtract(w2, w2mean)
    w2 = np.divide(w2, w2std)
    b1 = np.random.randn()
    b2 = np.random.randn()
    param_delta = 0.05
    param_lambda = 1e-3
    Y = data.class_to_onehot(Y_)

    for i in range(10000):

        # w1mean = np.mean(w1)
        # w1 = np.subtract(w1, w1mean)
        # w2mean = np.mean(w2)
        # w2 = np.subtract(w2, w2mean)

        s1 = np.dot(X, w1) + b1
        h1 = np.maximum(0, s1)
        s2 = np.dot(h1, w2) + b2
        expscores = np.exp(s2)
        sumexp = np.sum(expscores, axis=1, keepdims=True)

        h2 = np.divide(expscores, sumexp)
        logprobs = np.log(h2)

        loss = - sum([logprobs[i][Y_[i]] for i in range(len(logprobs))]) / len(Y_)

        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        Gs2 = np.subtract(h2, Y) / len(X)
        gradW2 = np.dot(np.transpose(Gs2), h1)
        gradB2 = np.sum(Gs2, axis=0, keepdims=True)
        Gh1 = np.dot(Gs2, np.transpose(w2))
        Gh1[s1 < 0] = 0
        Gs1 = Gh1
        gradW1 = np.dot(np.transpose(Gs1), X)
        gradB1 = np.sum(Gs1, axis=0, keepdims=True)

        w1 += -param_delta * np.transpose(gradW1)
        b1 += -param_delta * gradB1
        w2 += -param_delta * np.transpose(gradW2)
        b2 += -param_delta * gradB2

    return w1, b1, w2, b2


def fcann2_classify(X, w1, b1, w2, b2):
    s1 = np.dot(X, w1) + b1
    h1 = np.maximum(0, s1)
    s2 = np.dot(h1, w2) + b2
    expscores = np.exp(s2)
    sumexp = np.sum(expscores, axis=1, keepdims=True)

    return np.divide(expscores, sumexp)


if __name__ == "__main__":
    np.random.seed(100)

    # get the training dataset
    X, Y_ = data.sample_gmm_2d(6, 2, 10)

    # train the model
    w1, b1, w2, b2 = fcann2_train(X, Y_)

    probs = fcann2_classify(X, w1, b1, w2, b2)
    Y = np.array([np.argmax(i) for i in probs])

    accuracy, recall, precision = data.eval_perf_multi(Y, Y_)
    print(accuracy, recall, precision)
