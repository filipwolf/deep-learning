import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

import data


class PTLogreg(nn.Module):
    def __init__(self, D, C):
        """Arguments:
           - D: dimensions of each datapoint
           - C: number of classes
        """

        super().__init__()
        self.W = nn.Parameter(torch.tensor(np.random.randn(D, C)))
        self.b = nn.Parameter(torch.zeros(C))

    def forward(self, X):
        return torch.softmax(torch.mm(X, self.W) + self.b, dim=1)

    # unaprijedni prolaz modela: izračunati vjerojatnosti
    #   koristiti: torch.mm, torch.softmax
    # ...

    def get_loss(self, X, Yoh_, param_lambda):
        return - torch.mean(torch.log(torch.sum(X * Yoh_, dim=1))) + param_lambda * torch.norm(self.W)

    # formulacija gubitka
    #   koristiti: torch.log, torch.mean, torch.sum
    # ...


def train(model, X, Yoh_, param_niter, param_delta, param_lambda):
    """Arguments:
       - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
       - param_delta: learning rate
    """
    optimizer = optim.SGD([model.W, model.b], lr=param_delta)

    for i in range(param_niter):
        # afin regresijski model
        Y_ = model.forward(X)

        loss = model.get_loss(Y_, Yoh_, param_lambda)

        # računanje gradijenata
        loss.backward()

        # korak optimizacije
        optimizer.step()

        # Postavljanje gradijenata na nulu
        optimizer.zero_grad()

        print(f'step: {i}, loss:{loss}')


def eval(model, X):
    """Arguments:
       - model: type: PTLogreg
       - X: actual datapoints [NxD], type: np.array
       Returns: predicted class probabilites [NxC], type: np.array
    """

    return torch.Tensor.numpy(torch.Tensor.detach(model.forward(X)))


def logreg_decfun(model):
    def classify(X):
        return eval(model, torch.tensor(X))

    return classify


if __name__ == "__main__":
    # inicijaliziraj generatore slučajnih brojeva
    np.random.seed(100)

    # instanciraj podatke X i labele Yoh_
    X1, Y_1 = data.sample_gmm_2d(3, 3, 100)
    Yoh_1 = data.class_to_onehot(Y_1)
    X = torch.tensor(X1)
    Yoh_ = torch.tensor(Yoh_1)

    # definiraj model:
    ptlr = PTLogreg(X.shape[1], Yoh_.shape[1])

    # nauči parametre (X i Yoh_ moraju biti tipa torch.Tensor):
    train(ptlr, X, Yoh_, 10000, 0.05, 0.5)

    # dohvati vjerojatnosti na skupu za učenje
    probs = eval(ptlr, X)
    Y = np.array([np.argmax(i) for i in probs])
    accuracy, recall, precision = data.eval_perf_multi(Y_1, Y)
    print(accuracy, recall, precision)

    decfun = logreg_decfun(ptlr)
    bbox = (np.min(X1, axis=0), np.max(X1, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X1, Y_1, Y, special=[])

    # show the plot
    plt.show()
