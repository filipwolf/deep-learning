import time
from pathlib import Path
from torchvision import transforms
from layers_PyTorch import train
import numpy as np
from torchvision.datasets import MNIST
import layers_PyTorch

DATA_DIR = Path(__file__).parent.parent / 'lab2' / 'datasets' / 'MNIST'
SAVE_DIR = Path(__file__).parent.parent / 'lab2' / 'out'

config = {'max_epochs': 8, 'batch_size': 50, 'save_dir': SAVE_DIR, 'weight_decay': 1e-3,
          'lr_policy': {1: {'lr': 1e-1}, 3: {'lr': 1e-2}, 5: {'lr': 1e-3}, 7: {'lr': 1e-4}}}


def dense_to_one_hot(y, class_count):
    return np.eye(class_count)[y]


# np.random.seed(100)
np.random.seed(int(time.time() * 1e6) % 2 ** 31)

ds_train, ds_test = MNIST(DATA_DIR, train=True, download=True, transform=transforms.Compose([
    transforms.ToTensor(),  # first, convert image to PyTorch tensor
    transforms.Normalize((0.1307,), (0.3081,))  # normalize inputs
])), MNIST(DATA_DIR, train=False, transform=transforms.Compose([
    transforms.ToTensor(),  # first, convert image to PyTorch tensor
    transforms.Normalize((0.1307,), (0.3081,))  # normalize inputs
]))

train_x = ds_train.data.reshape([-1, 1, 28, 28]).numpy().astype(float) / 255
test_x = ds_test.data.reshape([-1, 1, 28, 28]).numpy().astype(float) / 25
test_y = ds_test.targets.numpy()
train_mean = train_x.mean()
train_x, test_x = (x - train_mean for x in (train_x, test_x))

model = layers_PyTorch.CovolutionalModel(1, 16, 32, 1568, 512, 10)
model = model.cuda()

train(train_x, config, ds_train, model, ds_test)
