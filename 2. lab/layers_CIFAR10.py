import os
import torch
import math
from torch import nn
import numpy as np
from torch.autograd import Variable
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
import skimage as ski
import skimage.io
import torchvision as tv


class CovolutionalModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 16, kernel_size=5)
        self.maxPool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5)

        # potpuno povezani slojevi
        self.fc1 = nn.Linear(32 * 5 * 5, 256, bias=True)
        self.fc2 = nn.Linear(256, 128, bias=True)
        self.fc_logits = nn.Linear(128, 10, bias=True)

        # parametri su već inicijalizirani pozivima Conv2d i Linear
        # ali možemo ih drugačije inicijalizirati
        self.reset_parameters()

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear) and m is not self.fc_logits:
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
        self.fc_logits.reset_parameters()

    def forward(self, x):
        h = self.conv1(x)
        h = torch.relu(h)
        h = self.maxPool(h)
        h = self.conv2(h)
        h = torch.relu(h)
        h = self.maxPool(h)
        h = h.view(-1, 32 * 5 * 5)
        h = self.fc1(h)
        h = torch.relu(h)
        h = self.fc2(h)
        h = torch.relu(h)
        logits = self.fc_logits(h)

        return logits


def train(config, ds_train, model, ds_test):
    batch_size = config['batch_size']
    max_epochs = config['max_epochs']
    weight_decay = config['weight_decay']
    save_dir = config['save_dir']
    criterion = nn.CrossEntropyLoss()
    training_generator = DataLoader(ds_train, batch_size=batch_size, shuffle=True, num_workers=8)
    test_generator = DataLoader(ds_test, batch_size=batch_size, shuffle=True, num_workers=8)
    test_generator2 = DataLoader(ds_test, batch_size=10000, shuffle=True, num_workers=8)
    optim = torch.optim.SGD(model.parameters(), lr=1e-2, weight_decay=weight_decay, momentum=0.9)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optim, gamma=1 - 1e-4)
    model.train()

    draw_conv_filters(0, 0, model.conv1.weight.detach().cpu().numpy(), save_dir)
    for epoch in range(max_epochs):
        running_loss = 0
        for i, (data, label) in enumerate(training_generator):
            data, label = data.to('cuda'), label.to('cuda')
            data = Variable(data)
            target = Variable(label)

            preds = model(data)
            loss = criterion(preds, target)
            loss.backward()
            optim.step()
            optim.zero_grad()
            scheduler.step()

            running_loss += loss.item()
            if i % 10 == 49:
                print('[%d, %5d] loss: %.3f' %
                      (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0

        with torch.no_grad():
            for data in test_generator2:
                images, labels = data
                images, labels = images.to('cuda'), labels.to('cuda')
                outputs = model(images)
                _, predicted = torch.max(outputs.data, 1)
                a, b, c = eval_perf_multi(predicted.cpu(), labels.cpu())
                print(a)
                print(b)
                print(c)

    draw_conv_filters(0, 1, model.conv1.weight.detach().cpu().numpy(), save_dir)

    correct = 0
    total = 0
    wrong = []

    with torch.no_grad():
        for data in test_generator:
            images, labels = data
            images, labels = images.to('cuda'), labels.to('cuda')
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            k = 0
            for i, j in zip(predicted, labels):
                if i != j:
                    a = [images[k], i]
                    wrong.append(a)
                k += 1

    for i in range(20):
        print(wrong[i][1])
        draw_image(tv.utils.make_grid(wrong[i][0]))


def draw_conv_filters(epoch, step, weights, save_dir):
    w = weights.copy()
    num_filters = w.shape[0]
    num_channels = w.shape[1]
    k = w.shape[2]
    assert w.shape[3] == w.shape[2]
    w = w.transpose(2, 3, 1, 0)
    w -= w.min()
    w /= w.max()
    border = 1
    cols = 8
    rows = math.ceil(num_filters / cols)
    width = cols * k + (cols - 1) * border
    height = rows * k + (rows - 1) * border
    img = np.zeros([height, width, num_channels])
    for i in range(num_filters):
        r = int(i / cols) * (k + border)
        c = int(i % cols) * (k + border)
        img[r:r + k, c:c + k, :] = w[:, :, :, i]
    filename = 'epoch_%02d_step_%06d.png' % (epoch, step)
    ski.io.imsave(os.path.join(save_dir, filename), img)


def plot_training_progress(save_dir, data):
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(16, 8))

    linewidth = 2
    legend_size = 10
    train_color = 'm'
    val_color = 'c'

    num_points = len(data['train_loss'])
    x_data = np.linspace(1, num_points, num_points)
    ax1.set_title('Cross-entropy loss')
    ax1.plot(x_data, data['train_loss'], marker='o', color=train_color,
             linewidth=linewidth, linestyle='-', label='train')
    ax1.plot(x_data, data['valid_loss'], marker='o', color=val_color,
             linewidth=linewidth, linestyle='-', label='validation')
    ax1.legend(loc='upper right', fontsize=legend_size)
    ax2.set_title('Average class accuracy')
    ax2.plot(x_data, data['train_acc'], marker='o', color=train_color,
             linewidth=linewidth, linestyle='-', label='train')
    ax2.plot(x_data, data['valid_acc'], marker='o', color=val_color,
             linewidth=linewidth, linestyle='-', label='validation')
    ax2.legend(loc='upper left', fontsize=legend_size)
    ax3.set_title('Learning rate')
    ax3.plot(x_data, data['lr'], marker='o', color=train_color,
             linewidth=linewidth, linestyle='-', label='learning_rate')
    ax3.legend(loc='upper left', fontsize=legend_size)

    save_path = os.path.join(save_dir, 'training_plot.png')
    print('Plotting in: ', save_path)
    plt.savefig(save_path)


def draw_image(img):
    img = img / 2 + 0.5  # unnormalize
    npimg = img.cpu().numpy()  # convert from tensor
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


def eval_perf_multi(Y, Y_):
    pr = []
    n = max(Y_) + 1
    M = np.bincount(n * Y_ + Y, minlength=n * n).reshape(n, n)
    for i in range(n):
        tp_i = M[i, i]
        fn_i = np.sum(M[i, :]) - tp_i
        fp_i = np.sum(M[:, i]) - tp_i
        tn_i = np.sum(M) - fp_i - fn_i - tp_i
        recall_i = tp_i / (tp_i + fn_i)
        precision_i = tp_i / (tp_i + fp_i)
        pr.append((recall_i, precision_i))

    accuracy = np.trace(M) / np.sum(M)

    return accuracy, pr, M