from pathlib import Path
from torchvision.datasets import CIFAR10
import layers_CIFAR10
from layers_CIFAR10 import train
from torchvision import transforms

DATA_DIR = Path(__file__).parent.parent / 'lab2' / 'datasets' / 'CIFAR' / 'cifar-10-python' / 'cifar-10-batches-py'
SAVE_DIR = Path(__file__).parent.parent / 'lab2' / 'out'

ds_train, ds_test = CIFAR10(DATA_DIR, train=True, download=True, transform=transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])), \
                    CIFAR10(DATA_DIR, train=False, transform=transforms.Compose(
                        [transforms.ToTensor(),
                         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))

config = {'max_epochs': 8, 'batch_size': 50, 'save_dir': SAVE_DIR, 'weight_decay': 1e-3,
          'lr_policy': {1: {'lr': 1e-1}, 3: {'lr': 1e-2}, 5: {'lr': 1e-3}, 7: {'lr': 1e-4}}}

model = layers_CIFAR10.CovolutionalModel()
model = model.cuda()

train(config, ds_train, model, ds_test)
