import torch
import numpy as np
from torch import nn
from torch.utils.data import DataLoader
from torch.autograd import Variable
import torch.nn.functional as F
import torchvision
from torch.utils.tensorboard import SummaryWriter


class CovolutionalModel(nn.Module):
    def __init__(self, in_channels, conv1_width, conv2_width, fc1_input, fc1_width, class_count):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels, conv1_width, kernel_size=5, padding=2)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv2 = nn.Conv2d(conv1_width, conv2_width, kernel_size=5, padding=2)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        # potpuno povezani slojevi
        self.fc1 = nn.Linear(fc1_input, fc1_width, bias=True)
        self.fc_logits = nn.Linear(fc1_width, class_count, bias=True)

        # parametri su već inicijalizirani pozivima Conv2d i Linear
        # ali možemo ih drugačije inicijalizirati
        self.reset_parameters()

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear) and m is not self.fc_logits:
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
        self.fc_logits.reset_parameters()

    def forward(self, x):

        h = F.relu(self.pool1(self.conv1(x)))
        h = F.relu(self.pool2(self.conv2(h)))
        h = h.view(h.shape[0], -1)
        h = self.fc1(h)
        h = torch.relu(h)
        logits = self.fc_logits(h)

        return logits

    def loss(self, x, y):
        return np.sum(np.subtract(np.log(np.sum(np.exp(x), axis=1)), np.sum(np.multiply(x, y), axis=1))) / len(y)


def train(train_x, config, ds_train, model, ds_test):
    writer = SummaryWriter()
    batch_size = config['batch_size']
    max_epochs = config['max_epochs']
    weight_decay = config['weight_decay']
    num_examples = train_x.shape[0]
    criterion = nn.CrossEntropyLoss()
    training_generator = DataLoader(ds_train, batch_size=batch_size, shuffle=True, num_workers=8)
    test_generator = DataLoader(ds_test, batch_size=batch_size, shuffle=True, num_workers=8)
    optim = torch.optim.SGD(model.parameters(), weight_decay=weight_decay, lr=0.01)
    model.train()
    for epoch in range(max_epochs):
        cnt_correct = 0
        for i, (data, label) in enumerate(training_generator):
            data, label = data.to('cuda'), label.to('cuda')
            data = Variable(data)
            target = Variable(label)

            preds = model(data)
            loss = criterion(preds, target)
            loss.backward()
            optim.step()
            optim.zero_grad()

            yp = np.argmax(preds.detach().cpu().numpy(), axis=1)
            cnt_correct += (yp == label.detach().cpu().numpy()).sum()

            if i % 5 == 0:
                print("epoch %d, step %d/%d, batch loss = %.2f" % (epoch, i*batch_size, num_examples, loss))
                writer.add_scalar('loss', loss, i)
                writer.close()
            if i % 100 == 0:
                grid = torchvision.utils.make_grid(data)
                writer.add_image('images', grid, 0)
                writer.add_graph(model, data)
                writer.close()
                # draw_conv_filters(epoch, i*batch_size, net[3])

        correct = 0
        total = 0
        with torch.no_grad():
            for data in test_generator:
                images, labels = data
                images, labels = images.to('cuda'), labels.to('cuda')
                outputs = model(images)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()

        print('Accuracy of the network on the 10000 test images: %d %%' % (
                100 * correct / total))
