import numpy as np
import matplotlib.pyplot as plt

import data


def logreg_train(X, Y_):
    """
    Argumenti
      X:  podatci, np.array NxD
      Y_: indeksi razreda, np.array Nx1

    Povratne vrijednosti
      w, b: parametri logističke regresije
    """

    w = np.random.randn(2, max(Y_) + 1)
    b = 0
    param_delta = 0.1
    Y = class_to_onehot(Y_)

    # gradijentni spust (param_niter iteracija)
    for i in range(40000):
        # eksponencirane klasifikacijske mjere
        # pri računanju softmaksa obratite pažnju
        # na odjeljak 4.1 udžbenika
        # (Deep Learning, Goodfellow et al)!
        scores = np.dot(X, w) + b
        expscores = np.exp(scores)

        # nazivnik softmaksa
        sumexp = np.sum(expscores, axis=1, keepdims=True)

        # logaritmirane vjerojatnosti razreda
        probs = np.divide(expscores, sumexp)
        logprobs = np.log(probs)

        # gubitak
        loss = - sum([logprobs[i][Y_[i]] for i in range(len(logprobs))])

        # dijagnostički ispis
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        # derivacije komponenata gubitka po mjerama
        dL_ds = np.transpose(np.subtract(probs, Y))

        # gradijenti parametara
        grad_W = np.transpose(np.dot(dL_ds, X)) / Y_.size
        grad_b = np.sum(dL_ds, axis=1) / Y_.size

        # poboljšani parametri
        w += -param_delta * grad_W
        b += -param_delta * grad_b

    return w, b


def logreg_classify(X, w, b):
    """
      Argumenti
          X:    podatci, np.array NxD
          w, b: parametri logističke regresije

      Povratne vrijednosti
          probs: vjerojatnosti razreda c1
    """
    scores = np.dot(X, w) + b
    expscores = np.exp(scores)
    sumexp = sum(np.transpose(expscores))
    return np.array([np.array([expscores[i][j] / sumexp[i] for j in range(max(Y_) + 1)]) for i in range(len(sumexp))])


def logreg_decfun(w, b):
    def classify(X):
        return logreg_classify(X, w, b)

    return classify


def class_to_onehot(Y):
    Yoh = np.zeros((len(Y), max(Y)+1))
    Yoh[range(len(Y)), Y] = 1
    return Yoh


if __name__ == "__main__":
    np.random.seed(100)

    # get the training dataset
    X, Y_ = data.sample_gmm_2d(3, 3, 100)

    # train the model
    w, b = logreg_train(X, Y_)

    # evaluate the model on the training dataset
    probs = logreg_classify(X, w, b)
    Y = np.array([np.where(i == np.amax(i))[0][0] for i in probs])

    # report performance
    accuracy, recall, precision = data.eval_perf_multi(Y, Y_)
    print(accuracy, recall, precision)

    # graph the decision surface
    decfun = logreg_decfun(w, b)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X, Y_, Y, special=[])

    # show the plot
    plt.show()
