import numpy as np
import matplotlib.pyplot as plt

import data


def binlogreg_train(X, Y_):
    """
    Argumenti
      X:  podatci, np.array NxD
      Y_: indeksi razreda, np.array Nx1

    Povratne vrijednosti
      w, b: parametri logističke regresije
    """

    w = np.random.randn(2, 1)
    b = 0
    param_delta = 0.1

    # gradijentni spust (param_niter iteracija)
    for i in range(4000):
        # klasifikacijske mjere
        scores = np.dot(X, w) + b

        # vjerojatnosti razreda c_1
        probs = np.array([np.exp(scores[i]) / (1 + np.exp(scores[i])) for i in range(Y_.size)])

        # gubitak
        loss = - np.sum([np.log(probs[i]) if Y_[i] == 0 else np.log(1 - probs[i]) for i in range(len(probs))])

        # dijagnostički ispis
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        # derivacije gubitka po klasifikacijskim mjerama
        dL_dscores = np.transpose(probs) - np.array([1 if i == 0 else 0 for i in Y_])

        # gradijenti parametara
        grad_w = np.dot(dL_dscores, X) / Y_.size
        grad_b = np.sum(dL_dscores) / Y_.size

        # poboljšani parametri
        w += -param_delta * np.transpose(grad_w)
        b += -param_delta * grad_b

    return w, b


def binlogreg_classify(X, w, b):
    """
      Argumenti
          X:    podatci, np.array NxD
          w, b: parametri logističke regresije

      Povratne vrijednosti
          probs: vjerojatnosti razreda c1
    """
    score = np.dot(X, w) + b
    return np.exp(score) / (1 + np.exp(score))


def binlogreg_decfun(w, b):
    def classify(X):
        return binlogreg_classify(X, w, b)

    return classify


if __name__ == "__main__":
    np.random.seed(100)

    # get the training dataset
    X, Y_ = data.sample_gauss_2d(2, 100)

    # train the model
    w, b = binlogreg_train(X, Y_)

    # evaluate the model on the training dataset
    probs = binlogreg_classify(X, w, b)
    Y = np.array([1 if i < 0.5 else 0 for i in probs])

    # report performance
    accuracy, recall, precision = data.eval_perf_binary(Y, Y_)
    AP = data.eval_AP(Y_[probs.argsort()])
    print(accuracy, recall, precision, AP)

    # graph the decision surface
    decfun = binlogreg_decfun(w, b)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X, Y_, Y, special=[])

    # show the plot
    plt.show()
